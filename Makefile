TARGET_STATIC    = libxcp.a
TARGET_DYNAMIC   = libxcp.so
SRCDIR    = src
CC		  = gcc
CFLAGS    = -std=c17 -fPIC
LDFLAGS   = -lz -lcrypto
SRCS      = $(foreach dir,$(SRCDIR), $(wildcard $(dir)/*.c))
OBJS      = $(SRCS:.c=.o)

platform_id != uname -s

platform != if [ $(platform_id) = Linux ] || \
    [ $(platform_id) = FreeBSD ] || \
    [ $(platform_id) = OpenBSD ] || \
    [ $(platform_id) = NetBSD ] || \
    [ $(platform_id) = Darwin ]; then \
        echo $(platform_id); \
    else \
        echo Unrecognized; \
    fi

ifeq ($(platform),FreeBSD)
    CFLAGS   += -D_FILE_OFFSET_BITS=64
endif
ifeq ($(platform),Darwin)
    CFLAGS   += -D_FILE_OFFSET_BITS=64
endif
ifeq ($(platform),Linux)
    CFLAGS   += -D_FILE_OFFSET_BITS=64
endif

%.o: %.c ; $(CC) -c $(CFLAGS) $< -o $@

$(TARGET_DYNAMIC): $(OBJS)
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -shared -o $@

$(TARGET_STATIC): $(OBJS)
	ar rcs $@ $^

clean:
	rm $(OBJS) libxcp.*