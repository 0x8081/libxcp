#ifndef INFLATE_H
#define INFLATE_H

#include "util.h"
#include <zlib.h>

#define CHUNK 256 * 1024         /*256KB Decompress*/
#define HEADER_SIZE 44 * 1024    /*SVOD Header 44KB*/
#define DATA_BLOCK_SIZE 170459136  /*Data Block ~162MB*/
#define DATA_SIZE 1024 * 1024      /*1MB R/W Buffer*/

int inf(z_stream *strm, FILE *source, void *buf);
void zerr(int ret);

#endif