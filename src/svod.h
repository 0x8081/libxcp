#ifndef SVOD_H
#define SVOD_H

#include "util.h"
#include "xcp.h"

EXPORT int svod_decompress(XCP_CTX *ctx);

#endif