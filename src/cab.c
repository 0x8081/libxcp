#include "cab.h"

void *mem_alloc(struct mspack_system *self, size_t bytes)
{
    /* put your memory allocator here */
    return malloc(bytes);
}

void mem_free(void *buffer)
{
    /* put your memory deallocator here */
    free(buffer);
}

void mem_copy(void *src, void *dest, size_t bytes)
{
    /* put your own memory copy routine here */
    memcpy(dest, src, bytes);
}

void mem_msg(struct mem_file *file, const char *format, ...)
{
    /* put your own printf-type routine here, or leave it empty */
}

struct mem_file *mem_open(struct mspack_system *self,
                          struct mem_buf *fn, int mode)
{
    struct mem_file *fh;
    if (!fn || !fn->data || !fn->length)
        return NULL;
    if ((fh = (struct mem_file *)mem_alloc(self, sizeof(struct mem_file))))
    {
        fh->data = (char *)fn->data;
        fh->length = fn->length;
        fh->posn = (mode == MSPACK_SYS_OPEN_APPEND) ? fn->length : 0;
    }
    return fh;
}

void mem_close(struct mem_file *fh)
{
    if (fh)
        mem_free(fh);
}

int mem_read(struct mem_file *fh, void *buffer, int bytes)
{
    int todo;
    if (!fh || !buffer || bytes < 0)
        return -1;
    todo = fh->length - fh->posn;
    if (todo > bytes)
        todo = bytes;
    if (todo > 0)
        mem_copy(&fh->data[fh->posn], buffer, (size_t)todo);
    fh->posn += todo;
    return todo;
}

int mem_write(struct mem_file *fh, void *buffer, int bytes)
{
    int todo;
    if (!fh || !buffer || bytes < 0)
        return -1;
    todo = fh->length - fh->posn;
    if (todo > bytes)
        todo = bytes;
    if (todo > 0)
        mem_copy(buffer, &fh->data[fh->posn], (size_t)todo);
    fh->posn += todo;
    return todo;
}

int mem_seek(struct mem_file *fh, off_t offset, int mode)
{
    if (!fh)
        return 1;
    switch (mode)
    {
    case MSPACK_SYS_SEEK_START:
        break;
    case MSPACK_SYS_SEEK_CUR:
        offset += (off_t)fh->posn;
        break;
    case MSPACK_SYS_SEEK_END:
        offset += (off_t)fh->length;
        break;
    default:
        return 1;
    }
    if ((offset < 0) || (offset > (off_t)fh->length))
        return 1;
    fh->posn = (size_t)offset;
    return 0;
}

off_t mem_tell(struct mem_file *fh)
{
    return (fh) ? (off_t)fh->posn : -1;
}