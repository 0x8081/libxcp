#include "stfs.h"

#include "cab.h"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <openssl/rc4.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>

#define XENON_DATA_SIZE 0x1C

static char str_live[] = "LIVE";

void XeCryptHmacSha(uint8_t *key, uint8_t *data, uint32_t len, uint8_t *digest)
{
    HMAC(EVP_sha1(), key, 16, data, len, digest, &len);
}

void decrypt(void *data, uint8_t *key, size_t rc4_sha_struct_offset, size_t data_offset, size_t len)
{
    RC4_KEY rc4_key;
    uint8_t digest[SHA_DIGEST_LENGTH];
    uint8_t temp[8];

    // RC4 SHA1 Header
    RC4_SHA_HEADER *rc4_header = (data + rc4_sha_struct_offset);

    // Setup RC4 Key
    XeCryptHmacSha(key, rc4_header->checksum, SHA_DIGEST_LENGTH, digest);
    RC4_set_key(&rc4_key, SHA_DIGEST_LENGTH, digest);
    RC4(&rc4_key, 8, rc4_header->confounder, temp);

    // Decrypt
    RC4(&rc4_key, len, (data + data_offset), (data + data_offset));
}

void decompress(XCP_CTX *ctx)
{
    struct mscab_decompressor *cabd;
    struct mscabd_cabinet *cab;
    struct mscabd_file *file;

    struct mem_buf source = {ctx->data, ctx->size_c};
    struct mem_buf output;

    int ret;

    cabd = mspack_create_cab_decompressor(&mem_system);
    if (cab = cabd->open(cabd, (char *)&source))
    {
        // First Pass - Get total decompressed size
        for (file = cab->files; file; file = file->next)
        {
            ctx->size_d += file->length;
        }

        // Decompression buffer
        void *decompressed = calloc(1, ctx->size_d);
        size_t offset = 0;

        // Decompress all files into our buffer
        for (file = cab->files; file; file = file->next)
        {
            output.data = (decompressed + offset);
            output.length = file->length;

            cabd->extract(cabd, file, (char *)&output);

            memcpy(decompressed + offset, output.data, output.length);

            offset += output.length;
        }

        free(ctx->data);
        ctx->data = decompressed;
    }
    else
    {
        fprintf(stderr, "Failed to open cab stream\n");
    }
    mspack_destroy_cab_decompressor(cabd);
}

void stfs_decrypt(XCP_CTX *ctx, char *key)
{
    uint8_t key_hex[16];
    CAB_HEADER *cab_hdr;

    // Load Encrypted XCP into RAM
    ctx->data = calloc(1, ctx->size_c);
    FSEEK64(ctx->f, 0, SEEK_SET);
    fread(ctx->data, ctx->size_c, 1, ctx->f);
    FSEEK64(ctx->f, 0, SEEK_SET);

    // Convert Key string to bytes
    for (int i = 0; i < 16; i++)
        sscanf(key + 2 * i, "%02x", &key_hex[i]);

    // Decrypt CAB Header
    decrypt(ctx->data, key_hex, 0x60, 0, 0x60);
    cab_hdr = ctx->data;

    if (cab_hdr->magic != CAB_MAGIC)
    {
        fprintf(stderr, "Invalid Cabinet Header, wrong key?\n");
        return;
    }

    // Decrypt Folders
    size_t folder_size = cab_hdr->count_folders * sizeof(CAB_FOLDER);
    decrypt(ctx->data, key_hex, 0x28, 0x180, folder_size);

    // Decrypt Filenames
    decrypt(ctx->data, key_hex, 0x44, cab_hdr->off_files, cab_hdr->count_files * (sizeof(CAB_ENTRY) + 9));

    // Parse Folders
    size_t offset = 0x180;
    CAB_FOLDER *folders = ctx->data + offset;
    for (int i = 0; i < cab_hdr->count_folders; i++)
    {
        size_t size = 0;

        if (i + 1 == cab_hdr->count_folders)
            size = ctx->size_c - folders[i].off_cab_start;
        else
            size = folders[i + 1].off_cab_start - folders[i].off_cab_start;

        size_t off = (offset + (sizeof(CAB_FOLDER) * (i + 1)) - XENON_DATA_SIZE);
        decrypt(ctx->data, key_hex, off, folders[i].off_cab_start, size);
    }

    decompress(ctx);

    memcpy(ctx->data, str_live, 4);

    xcp_read_header(ctx);
    xcp_print_header(ctx);

    xcp_mkdir(ctx);

    FILE *f = fopen(ctx->path_data, "wb+");
    fwrite(ctx->data, ctx->size_d, 1, f);
    fclose(f);
}
