#ifndef CAB_H
#define CAB_H

#include "util.h"

#include <mspack.h>

#define CAB_MAGIC 0x4643534D

typedef struct _RC4_SHA_HEADER
{
    uint8_t checksum[0x14];
    uint8_t confounder[8];
} RC4_SHA_HEADER;

typedef struct _CAB_HEADER
{
    uint32_t magic;
    uint32_t checksum_header;
    uint32_t cb_cabinet;
    uint32_t checksum_folders;
    uint32_t off_files;
    uint32_t checksum_files;

    uint16_t version;
    uint16_t count_folders;
    uint16_t count_files;
    uint16_t count_flags;
    uint16_t flags;
    uint16_t set_it;
    uint16_t i_cabinet;
} CAB_HEADER;

typedef struct _CAB_FOLDER
{
    uint32_t off_cab_start;
    uint16_t cf_data;
    uint16_t type_compress;
    RC4_SHA_HEADER rc4_header;
} CAB_FOLDER;

typedef struct _CAB_ENTRY
{
    uint32_t cb_file;
    uint32_t off_folder_start;

    uint16_t i_folder;
    uint16_t date;
    uint16_t time;
    uint16_t attribs;
} CAB_ENTRY;

typedef struct _CAB_DATA
{
    uint32_t checksum;
    uint16_t cb_data;
    uint16_t cb_uncomp;
} CAB_DATA;

/* use a pointer to a mem_buf structure as "filenames" */
struct mem_buf
{
    void *data;
    size_t length;
};

struct mem_file
{
    char *data;
    size_t length, posn;
};

void *mem_alloc(struct mspack_system *self, size_t bytes);
void mem_free(void *buffer);
void mem_copy(void *src, void *dest, size_t bytes);
void mem_msg(struct mem_file *file, const char *format, ...);
struct mem_file *mem_open(struct mspack_system *self, struct mem_buf *fn, int mode);
void mem_close(struct mem_file *fh);
int mem_read(struct mem_file *fh, void *buffer, int bytes);
int mem_write(struct mem_file *fh, void *buffer, int bytes);
int mem_seek(struct mem_file *fh, off_t offset, int mode);
off_t mem_tell(struct mem_file *fh);

static struct mspack_system mem_system = {
    (struct mspack_file * (*)(struct mspack_system *, const char *, int)) & mem_open,
    (void (*)(struct mspack_file *)) & mem_close,
    (int (*)(struct mspack_file *, void *, int)) & mem_read,
    (int (*)(struct mspack_file *, void *, int)) & mem_write,
    (int (*)(struct mspack_file *, off_t, int)) & mem_seek,
    (off_t(*)(struct mspack_file *)) & mem_tell,
    (void (*)(struct mspack_file *, const char *, ...)) & mem_msg,
    &mem_alloc,
    &mem_free,
    &mem_copy,
    NULL};

#endif