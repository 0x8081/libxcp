#ifndef UTIL_H
#define UTIL_H

#include <errno.h>
#include <locale.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#ifdef _WIN32
#include <direct.h>
#include <intrin.h>
#define EXPORT __declspec(dllexport)
#define MKDIR _mkdir
#define BYTESWAP16 _byteswap_ushort
#define BYTESWAP32 _byteswap_ulong
#define BYTESWAP64 _byteswap_uint64
#define FOPEN64 fopen64
#define FTELL64 _ftelli64
#define FSEEK64 _fseeki64
#else
#include <sys/stat.h>
#include <unistd.h>
#define EXPORT __attribute__((visibility("default")))
#define MKDIR(X) mkdir(X, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#ifdef __ORDER_LITTLE_ENDIAN__
#define BYTESWAP16 __builtin_bswap16
#define BYTESWAP32 __builtin_bswap32
#define BYTESWAP64 __builtin_bswap64
#else
#define BYTESWAP16
#define BYTESWAP32
#define BYTESWAP64
#endif
#if defined(__FreeBSD__) || defined(__APPLE__)
#define FOPEN64 fopen
#define FTELL64 ftell
#define FSEEK64 fseek
#else
#define FOPEN64 fopen
#define FTELL64 ftell
#define FSEEK64 fseek
#endif
#endif

#define MAX_PATH 256

void byteswap16_utf_wchar(wchar_t *dest, const uint16_t *src, size_t size);
void narrow(char *dest, const wchar_t *src, size_t size);

#endif