#include "svod.h"

int svod_decompress(XCP_CTX *ctx)
{
  char block_path[256];
  uint8_t block_id = 0;
  uint64_t last_pos = ctx->strm.total_in;
  size_t data_written = 0;
  int ret;

  printf("XCP Contains SVOD. Splitting into SVOD Format...\n");

  xcp_mkdir(ctx);

  printf("Writting SVOD Header...\n");

  snprintf(ctx->path_header, MAX_PATH, "%s/%s", ctx->path_out, ctx->content_id_str);
  FILE *header = fopen(ctx->path_header, "wb+");
  fwrite(ctx->data, HEADER_SIZE, 1, header);
  fclose(header);

  printf("Writing Data%04d...\n", block_id);

  snprintf(block_path, MAX_PATH, "%s/Data%04d", ctx->path_data, block_id);
  FILE *block = fopen(block_path, "wb+");

  if (block == NULL)
    printf("COULD NOT OPEN %s\n!", block_path);

  fwrite(ctx->data + HEADER_SIZE, (DATA_SIZE)-HEADER_SIZE, 1, block);
  data_written += (DATA_SIZE)-HEADER_SIZE;

  while (last_pos < ctx->size_c)
  {
    FSEEK64(ctx->f, last_pos, SEEK_SET);
    memset(ctx->data, 0, DATA_SIZE);
    ret = inf(&ctx->strm, ctx->f, ctx->data);
    if (ret != Z_OK)
      zerr(ret);

    if (data_written + (DATA_SIZE) >= DATA_BLOCK_SIZE)
    {
      fwrite(ctx->data, DATA_BLOCK_SIZE - data_written, 1, block);
      fclose(block);
      block_id++;

      printf("Writing Data%04d...\n", block_id);
      snprintf(block_path, MAX_PATH, "%s/Data%04d", ctx->path_data, block_id);
      block = fopen(block_path, "wb+");

      if (DATA_BLOCK_SIZE - data_written < DATA_SIZE)
      {
        fwrite(ctx->data + (DATA_BLOCK_SIZE - data_written),
               (DATA_SIZE) - (DATA_BLOCK_SIZE - data_written), 1, block);
      }

      data_written = DATA_SIZE - (DATA_BLOCK_SIZE - data_written);
    }
    else
    {
      fwrite(ctx->data, DATA_SIZE, 1, block);
      data_written += DATA_SIZE;
    }

    last_pos += ctx->strm.total_in;
  }

  fclose(block);

  return ret;
}
