#ifndef STFS_H
#define STFS_H

#include "xcp.h"

EXPORT void stfs_decrypt(XCP_CTX *ctx, char *key);

#endif