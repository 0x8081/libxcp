#include "xcp.h"

enum XCP_TYPE xcp_check_magic(XCP_CTX *ctx)
{
  if (ctx->f == NULL)
  {
    printf("%s\n", strerror(errno));
    return XCP_INVALID;
  }

  uint16_t magic;
  if (fread(&magic, 2, 1, ctx->f) == 0)
    fprintf(stderr, "Failed reading file\n");

  if (BYTESWAP16(magic) != XCP_MAGIC)
  {
    fprintf(stderr, "\nPotentially Encrypted XCP, Trying STFS...\n");
    return XCP_STFS;
  }

  return XCP_SVOD;
}

int xcp_read_header(XCP_CTX *ctx)
{
  int ret = 0;

  if (ctx->type == XCP_SVOD)
  {
    fseek(ctx->f, 0, SEEK_SET);
    ret = inf(&ctx->strm, ctx->f, ctx->data);
    if (ret != Z_OK)
      zerr(ret);
  }

  // ctx->type = BYTESWAP32(*(uint32_t *)(ctx->data + 0x3A9));
  ctx->content_type = BYTESWAP32(*(uint32_t *)(ctx->data + 0x344));
  ctx->size_d = ctx->size_d != 0 ? ctx->size_d : (*(uint64_t *)(ctx->data + 0x3A1));

  uint16_t title_buff[0x40];
  memcpy(title_buff, (ctx->data + 0x1691), 0x80);
  byteswap16_utf_wchar(ctx->title, title_buff, 0x40);

  ctx->title_id = BYTESWAP32(*(uint32_t *)(ctx->data + 0x360));
  memcpy(ctx->content_id, ctx->data + 0x32C, 20);

  for (int i = 0; i < 20; i++)
    snprintf(ctx->content_id_str + (i * 2), 41, "%02X", ctx->content_id[i]);

  return ret;
}

void xcp_print_header(XCP_CTX *ctx)
{
  setlocale(LC_ALL, "");
  printf("%-24.24s %s\n", "Content ID:", ctx->content_id_str);
  printf("%-24.24s %ls\n", "Title:", ctx->title);
  printf("%-24.24s %08X\n", "TitleID:", ctx->title_id);
  printf("%-24.24s %08X\n",
         "MediaID:", BYTESWAP32(*(uint32_t *)(ctx->data + 0x354)));
  printf("%-24.24s %lu MB\n", "Compressed Size:", ctx->size_c / (DATA_SIZE));
  printf("%-24.24s %lu MB\n\n", "Decompressed Size:",
         (ctx->size_d + (ctx->type ? HEADER_SIZE : 0)) / (DATA_SIZE));
}

void xcp_mkdir(XCP_CTX *ctx)
{
  snprintf(ctx->path_out, MAX_PATH, "%8X", ctx->title_id);
  MKDIR(ctx->path_out);

  snprintf(ctx->path_out, MAX_PATH, "%8X/%07X", ctx->title_id, ctx->content_type);
  MKDIR(ctx->path_out);

  if (ctx->type == XCP_SVOD)
  {
    printf("Creating Data Directory...\n");
    snprintf(ctx->path_data, MAX_PATH, "%s/%s%s", ctx->path_out, ctx->content_id_str,
             ".data");
    MKDIR(ctx->path_data);
  }
  else
  {
    snprintf(ctx->path_data, MAX_PATH, "%s/%s", ctx->path_out, ctx->content_id_str);
  }
}

XCP_CTX *xcp_init(char *path)
{
  XCP_CTX *ctx = calloc(1, sizeof(XCP_CTX));
  strncpy(ctx->path, path, MAX_PATH);
  ctx->f = FOPEN64(ctx->path, "rb");
  ctx->type = xcp_check_magic(ctx);
  if (ctx->type == XCP_INVALID)
    return NULL;

  FSEEK64(ctx->f, 0, SEEK_END);
  ctx->size_c = FTELL64(ctx->f);
  FSEEK64(ctx->f, 0, SEEK_SET);

  ctx->data_size = 0;
  ctx->data = calloc(1, DATA_SIZE);

  return ctx;
}

void xcp_free(XCP_CTX *ctx)
{
  free(ctx->data);
  fclose(ctx->f);
  free(ctx);
}