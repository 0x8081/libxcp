#ifndef XCP_H
#define XCP_H

#include "inflate.h"
#include "util.h"

#define XCP_MAGIC 0x789C

enum CONTENT_TYPE {
    SAVED_GAME          = 1,
    MARKETPLACE_CONTENT = 2,
    PUBLISHER           = 3,
    XBOX_360_TITLE      = 0x0001000,
    IPTV_PAUSE_BUFFER   = 0x0002000,
    INSTALLED_GAME      = 0x0004000,
    XBOX_ORIGINAL_GAME  = 0x0005000,
    GAME_ON_DEMAND      = 0x0007000,
    AVATAR_ITEM         = 0x0009000,
    PROFILE             = 0x0010000,
    GAMER_PICTURE       = 0x0020000,
    THEME               = 0x0030000,
    CACHE_FILE          = 0x0040000,
    STORAGE_DOWNLOAD    = 0x0050000,
    XBOX_SAVED_GAME     = 0x0060000,
    XBOX_DOWNLOAD       = 0x0070000,
    GAME_DEMO           = 0x0080000,
    VIDEO               = 0x0090000,
    GAME_TITLE          = 0x00A0000,
    INSTALLER           = 0x00B0000,
    GAME_TRAILER        = 0x00C0000,
    ARCADE_TITLE        = 0x00D0000,
    XNA                 = 0x00E0000,
    LICENSE_STORE       = 0x00F0000,
    MOVIE               = 0x0100000,
    TV                  = 0x0200000,
    MUSIC_VIDEO         = 0x0300000,
    GAME_VIDEO          = 0x0400000,
    PODCAST_VIDEO       = 0x0500000,
    VIRAL_VIDEO         = 0x0600000,
    COMMUNITY_GAME      = 0x2000000,
};

enum XCP_TYPE
{
  XCP_STFS,
  XCP_SVOD,
  XCP_INVALID
};

// XCP Context
typedef struct _XCP_CTX
{
  // XCP Type (SVOD or STFS)
  enum XCP_TYPE type;

  // File Pointer
  FILE *f;
  // Path to XCP file
  char path[256];
  // Path to Output
  char path_out[256];
  // Extracted Header Path
  char path_header[256];
  // Extracted Data Directory
  char path_data[256];

  // Decompressed Data Buffer (1MB)
  void *data;
  // Size of Data Buffer
  size_t data_size;
  // Compressed Size
  size_t size_c;
  // Decompressed Size
  size_t size_d;
  // Zlib Stream Handle
  z_stream strm;

  // Title
  wchar_t title[64];
  // Title ID
  uint32_t title_id;
  // Content Type
  enum CONTENT_TYPE content_type;
  // Content ID / SHA-1
  uint8_t content_id[20];
  // Content ID / SHA-1 Hex String
  char content_id_str[41];

} XCP_CTX;

// Initialize XCP Context
EXPORT XCP_CTX *xcp_init(char *path);
EXPORT void xcp_free(XCP_CTX *ctx);
EXPORT int xcp_read_header(XCP_CTX *ctx);
EXPORT void xcp_print_header(XCP_CTX *ctx);

void xcp_mkdir(XCP_CTX *ctx);

#endif