#include "util.h"

void byteswap16_utf_wchar(wchar_t *dest, const uint16_t *src, size_t size) {
  for (int i = 0; i < size; i++)
    dest[i] = BYTESWAP16(src[i]);
}

void narrow(char *dest, const wchar_t *src, size_t size) {
  for (int i = 0; i < size; i++)
    dest[i] = (char)src[i];
}